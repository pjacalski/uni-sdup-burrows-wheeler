`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 17:43:14
// Design Name: 
// Module Name: sorter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sorter(
    input clk,
    input rst,
    input wire [63:0] m1, m2, m3, m4, m5, m6, m7, m8,
    output reg [7:0] idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8
    );
    
parameter CNT = 8;
reg [63:0] dat1, dat2, dat3, dat4, dat5, dat6, dat7, dat8;
always @(posedge clk)
begin
    dat1 <= m1;
    dat2 <= m2;
    dat3 <= m3;
    dat4 <= m4;
    dat5 <= m5;
    dat6 <= m6;
    dat7 <= m7;
    dat8 <= m8;
end
    
integer j, n;
reg [63:0] temp;
reg [7:0] idxTemp;
reg [63:0] array [1:8];
reg [7:0] idxArray [1:8];
reg [7:0] outidxArray[1:8];

always @*
begin
    array[1] = dat1; idxArray[1] = 1;
    array[2] = dat2; idxArray[2] = 2;
    array[3] = dat3; idxArray[3] = 3;
    array[4] = dat4; idxArray[4] = 4;
    array[5] = dat5; idxArray[5] = 5;
    array[6] = dat6; idxArray[6] = 6;
    array[7] = dat7; idxArray[7] = 7;
    array[8] = dat8; idxArray[8] = 8;


    for (n = CNT; n > 1; n = n - 1) begin
        for (j = 1 ; j < n; j = j + 1) begin
            if (array[j] > array[j + 1])
            begin
              temp = array[j];
              array[j] = array[j + 1];
              array[j + 1] = temp;
              
              idxTemp = idxArray[j];
              idxArray[j] = idxArray[j + 1];
              idxArray[j + 1] = idxTemp;
            end 
         end
    end
    
    for (j = 1; j <= CNT; j = j + 1) begin
        outidxArray[idxArray[j]] = j;
    end
    
end
    
always @(posedge clk)
begin
    idx1 <= outidxArray[1];
    idx2 <= outidxArray[2];
    idx3 <= outidxArray[3];
    idx4 <= outidxArray[4];
    idx5 <= outidxArray[5];
    idx6 <= outidxArray[6];
    idx7 <= outidxArray[7];
    idx8 <= outidxArray[8];
end
endmodule
