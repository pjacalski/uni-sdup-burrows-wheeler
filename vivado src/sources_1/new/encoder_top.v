`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 23:42:31
// Design Name: 
// Module Name: encoder_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module encoder_top(
    input clk,
    input rst,
    input [63:0] inStr,
    output [63:0] encodedStr,
    output [7:0] primaryIdx
    );
    
wire [7:0] lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8;
wire [63:0] metric1, metric2, metric3, metric4, metric5, metric6, metric7, metric8;
wire [7:0] idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8;
    
shiftAndGetLast shift_inst1(clk, rst, inStr, lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8); 
                                            
metricCalc metric_shift0(clk, rst, 0, inStr, metric1);
metricCalc metric_shift1(clk, rst, 1, inStr, metric2);
metricCalc metric_shift2(clk, rst, 2, inStr, metric3);
metricCalc metric_shift3(clk, rst, 3, inStr, metric4);
metricCalc metric_shift4(clk, rst, 4, inStr, metric5);
metricCalc metric_shift5(clk, rst, 5, inStr, metric6);
metricCalc metric_shift6(clk, rst, 6, inStr, metric7);
metricCalc metric_shift7(clk, rst, 7, inStr, metric8);

sorter sort_inst1(  clk, rst, 
                    metric1, metric2, metric3, metric4, metric5, metric6, metric7, metric8, 
                    idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8);

encoder encoder_inst1(  clk, rst, 
                        lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8,
                        idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8,
                        primaryIdx, encodedStr);
    
endmodule
