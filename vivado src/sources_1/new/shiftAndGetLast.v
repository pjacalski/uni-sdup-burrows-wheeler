`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 22:03:16
// Design Name: 
// Module Name: shiftAndGetLast
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module shiftAndGetLast(
    input clk,
    input rst,
    input wire [63:0] str,
    output wire [7:0] lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8
    );
    
parameter BYTE = 8;
    
assign lb1 = str[ 1 * BYTE - 1 : BYTE * 0 ];
assign lb2 = str[ 8 * BYTE - 1 : BYTE * 7 ];
assign lb3 = str[ 7 * BYTE - 1 : BYTE * 6 ];
assign lb4 = str[ 6 * BYTE - 1 : BYTE * 5 ];
assign lb5 = str[ 5 * BYTE - 1 : BYTE * 4 ];
assign lb6 = str[ 4 * BYTE - 1 : BYTE * 3 ];
assign lb7 = str[ 3 * BYTE - 1 : BYTE * 2 ];
assign lb8 = str[ 2 * BYTE - 1 : BYTE * 1 ];
    
endmodule
