`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 22:07:19
// Design Name: 
// Module Name: encoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

/**
    Encoder
    @input      clk         clock
    @input      rst         synchronous reset
    @input      lb[1..8]    last character of input string after shift of 0..7
    @input      idx[1..8]   index of last character with shift 0..7 in encoded string (after sorting), first = 1
    @output     primIdx     index of first (primary) character of input string in encoded string
    @output     encStr      encoded string of 8 characters
*/
module encoder(
    input clk,
    input rst,
    input [7:0] lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8,
    input [7:0] idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8,
    output reg [7:0] primIdx,
    output reg [63:0] encStr
    );
    
parameter BYTE = 8;
    
reg [7:0] tmpArray[1:8];

always @ (posedge clk)
begin
    if (rst == 1)
    begin
       
    end
    else
    begin
       tmpArray[idx1] <= lb1;
       tmpArray[idx2] <= lb2;
       tmpArray[idx3] <= lb3;
       tmpArray[idx4] <= lb4;
       tmpArray[idx5] <= lb5;
       tmpArray[idx6] <= lb6;
       tmpArray[idx7] <= lb7;
       tmpArray[idx8] <= lb8;
    end
end
    
always @ (posedge clk)
begin
    if (rst == 1)
    begin
        primIdx = 0;
        encStr = 0;
    end
    else
    begin
        // Primary index is equal to index of first character of input string after encoding
        // First character is present in encoded string when input is shifted by 1 (lb2)
        // Actual (after sorting) index in encoded string is therfore idx2
        primIdx = idx2; 
        
        encStr[ 1*BYTE - 1 : 0*BYTE ] <= tmpArray[8];
        encStr[ 2*BYTE - 1 : 1*BYTE ] <= tmpArray[7];
        encStr[ 3*BYTE - 1 : 2*BYTE ] <= tmpArray[6];
        encStr[ 4*BYTE - 1 : 3*BYTE ] <= tmpArray[5];
        encStr[ 5*BYTE - 1 : 4*BYTE ] <= tmpArray[4];
        encStr[ 6*BYTE - 1 : 5*BYTE ] <= tmpArray[3];
        encStr[ 7*BYTE - 1 : 6*BYTE ] <= tmpArray[2];
        encStr[ 8*BYTE - 1 : 7*BYTE ] <= tmpArray[1];
    end
end

    
endmodule
