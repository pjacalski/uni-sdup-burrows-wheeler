`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 14:59:48
// Design Name: 
// Module Name: test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module metricCalc(
        input clk, rst,
        input [7:0] shift,
        input wire [63:0] str,
        output reg [63:0] metric,
        output reg rdy
    );
    
parameter LEN = 8;
parameter BYTE = 8;

reg [7:0] str_tmp[0:7];

always @ (posedge clk)
begin
    str_tmp[0] <= str[ 1*BYTE - 1 : 0*BYTE ];
    str_tmp[1] <= str[ 2*BYTE - 1 : 1*BYTE ];
    str_tmp[2] <= str[ 3*BYTE - 1 : 2*BYTE ];
    str_tmp[3] <= str[ 4*BYTE - 1 : 3*BYTE ];
    str_tmp[4] <= str[ 5*BYTE - 1 : 4*BYTE ];
    str_tmp[5] <= str[ 6*BYTE - 1 : 5*BYTE ];
    str_tmp[6] <= str[ 7*BYTE - 1 : 6*BYTE ];
    str_tmp[7] <= str[ 8*BYTE - 1 : 7*BYTE ];
end

always @ (posedge clk)
begin
if (rst == 1)
begin
    metric = 0;
    rdy = 0;
end
else 
begin
    metric[ 1*BYTE - 1 : 0*BYTE ] <= str_tmp[ (0 - shift) % LEN ];
    metric[ 2*BYTE - 1 : 1*BYTE ] <= str_tmp[ (1 - shift) % LEN ];
    metric[ 3*BYTE - 1 : 2*BYTE ] <= str_tmp[ (2 - shift) % LEN ];
    metric[ 4*BYTE - 1 : 3*BYTE ] <= str_tmp[ (3 - shift) % LEN ];
    metric[ 5*BYTE - 1 : 4*BYTE ] <= str_tmp[ (4 - shift) % LEN ];
    metric[ 6*BYTE - 1 : 5*BYTE ] <= str_tmp[ (5 - shift) % LEN ];
    metric[ 7*BYTE - 1 : 6*BYTE ] <= str_tmp[ (6 - shift) % LEN ];
    metric[ 8*BYTE - 1 : 7*BYTE ] <= str_tmp[ (7 - shift) % LEN ];
end
end                                  
    
endmodule
