`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 23:53:45
// Design Name: 
// Module Name: encoder_top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module encoder_top_tb();
    
    reg clk, rst;
    reg [7:0] str [1:8];
    reg [63:0] inStrPacked;
    wire [63:0] outStrPacked;
    wire [7:0] primIdx;
    
    encoder_top inst1(clk, rst, inStrPacked, outStrPacked, primIdx);
    
    //Clock generator
    initial
     clk <= 1'b1;
    always
     #5 clk <= ~clk;
    
    //Rst
    initial
    begin
    #5  rst = 0;
    #5  rst = 1;
    #5  rst = 0;
    end
    
    //Stimuli signals
    initial
    begin
      #50
      str[1] = "H";
      str[2] = "e";
      str[3] = "r";
      str[4] = "p";
      str[5] = "d";
      str[6] = "e";
      str[7] = "r";
      str[8] = "p";
      inStrPacked = {str[1], str[2], str[3], str[4], str[5], str[6], str[7], str[8]};
    end
    
endmodule
