`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 18:26:21
// Design Name: 
// Module Name: sorter_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module sorter_tb;

reg clk, rst;
reg [63:0] m1, m2, m3, m4, m5, m6, m7, m8;
wire [7:0] idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8;

sorter sort(clk, rst, m1, m2, m3, m4, m5, m6, m7, m8, idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8 );

//Clock generator
initial
 clk <= 1'b1;
always
 #5 clk <= ~clk;

//Rst
initial
begin
#5  rst <= 0;
#5  rst <= 1;
#5  rst <= 0;
end

//Stimuli signals
initial
begin
#50
 m1 = 1; // 0
 m2 = 2;
 m3 = 3;
 m4 = 4;
 m5 = 64'hFF000000;
 m6 = 64'hFF000001;
 m7 = 64'hFF000002;
 m8 = 64'hFF000003; 
#50
 m1 = 4; // 1
 m2 = 3; // 2
 m3 = 2; // 3
 m4 = 1; // 4
 m5 = 64'hFF000000; 
 m6 = 64'hFF000001; 
 m7 = 64'hFF000002; 
 m8 = 64'hFF000003; 
 #50
  m1 = 2; // 1
  m2 = 3; // 2
  m3 = 4; // 3
  m4 = 1; // 4
  m5 = 64'hFF000000; 
  m6 = 64'hFF000001; 
  m7 = 64'hFF000002; 
  m8 = 64'hFF000003; 
 
end


endmodule