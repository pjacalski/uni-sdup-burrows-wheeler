`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 15:12:32
// Design Name: 
// Module Name: test_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module matricCalc_tb;

reg clk, rst;
reg [7:0] shift;
reg [7:0] str [1:8];
reg [63:0] strPacked;
//reg [7:0] t1, t2, t3, t4, t5, t6, t7, t8;
wire [63:0] metric;
wire rdy;

metricCalc clc(clk, rst, shift, strPacked, metric, rdy);

//Clock generator
initial
 clk <= 1'b1;
always
 #5 clk <= ~clk;

//Rst
initial
begin
#5  rst = 0;
#5  rst = 1;
#5  rst = 0;
end

//Stimuli signals
initial
begin
  #50
  str[1] = "A";
  str[2] = "B";
  str[3] = "C";
  str[4] = "D";
  str[5] = "E";
  str[6] = "F";
  str[7] = "G";
  str[8] = "H";
  strPacked = {str[1], str[2], str[3], str[4], str[5], str[6], str[7], str[8]};
  
  shift = 2;
  #100
  shift = 4;
  #100
  shift = 6;
end


endmodule
