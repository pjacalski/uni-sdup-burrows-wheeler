`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.09.2018 00:34:21
// Design Name: 
// Module Name: encoder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module encoder_tb( );
    
reg clk, rst;
reg [7:0] lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8;
reg [7:0] idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8;
wire [7:0] primIdx;
wire [63:0] encStr;

encoder inst1(clk, rst, 
    lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, 
    idx1, idx2, idx3, idx4, idx5, idx6, idx7, idx8,
    primIdx, encStr);

//Clock generator
initial
 clk <= 1'b1;
always
 #5 clk <= ~clk;

//Rst
initial
begin
#5  rst = 0;
#5  rst = 1;
#5  rst = 0;
end

//Stimuli signals
initial
begin
  #50
  lb1 = "A"; idx1 = 1;
  lb2 = "B"; idx2 = 2;
  lb3 = "C"; idx3 = 3;
  lb4 = "D"; idx4 = 4;
  lb5 = "E"; idx5 = 5;
  lb6 = "F"; idx6 = 6;
  lb7 = "G"; idx7 = 7;
  lb8 = "H"; idx8 = 8;
  
  #50
  idx1 = 8;
  idx2 = 7;
  idx3 = 6;
  idx4 = 5;
  idx5 = 4;
  idx6 = 3;
  idx7 = 2;
  idx8 = 1;
end
    
endmodule
