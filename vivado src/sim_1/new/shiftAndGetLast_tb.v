`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.09.2018 22:18:18
// Design Name: 
// Module Name: shiftAndGetLast_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module shiftAndGetLast_tb( );

reg clk, rst;
reg [7:0] str [1:8];
reg [63:0] strPacked;
wire [7:0] lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8;

shiftAndGetLast int1(clk, rst, strPacked, lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8);

//Clock generator
initial
 clk <= 1'b1;
always
 #5 clk <= ~clk;

//Rst
initial
begin
#5  rst = 0;
#5  rst = 1;
#5  rst = 0;
end

initial
begin
#50
str[1] = "1";
str[2] = "2";
str[3] = "3";
str[4] = "4";
str[5] = "5";
str[6] = "6";
str[7] = "7";
str[8] = "8";
strPacked = {str[1], str[2], str[3], str[4], str[5], str[6], str[7], str[8]};

end

endmodule
