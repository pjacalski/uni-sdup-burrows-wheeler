from unittest import TestCase
from bwt import bwt_decode


class TestBwt_decode(TestCase):
    def test_bwt_decode(self):
        in_str = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
        enc_str = 'rt,mgmrtt  s ea s rmtncd psleoueaiodclLiiuooin pieicest'
        index = 8

        dec_str = bwt_decode(enc_str, index)

        if not dec_str == in_str:
            self.fail()
