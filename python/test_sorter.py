from unittest import TestCase
from bwt import sorter


class TestSorter(TestCase):
    def test_sorter(self):
        input_table = [3, 8, 4, 2, 5, 1, 6, 7]
        correct_id = [3, 8, 4, 2, 5, 1, 6, 7]

        return_id = sorter(input_table)[1]

        if not return_id == correct_id:
            self.fail()

        input_table = [x * 2 for x in input_table]

        return_id = sorter(input_table)[1]

        if not return_id == correct_id:
            self.fail()

        input_table = [69, 11, 23, 75, 55, 34, 80, 9]
        correct_id = [6, 2, 3, 7, 5, 4, 8, 1]
        return_id = sorter(input_table)[1]

        if not return_id == correct_id:
            self.fail()

        input_table = [12, 13, 64, 42, 87, 17, 3, 31]
        correct_id = [2, 3, 7, 6, 8, 4, 1, 5]
        return_id = sorter(input_table)[1]

        if not return_id == correct_id:
            self.fail()
