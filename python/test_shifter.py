from unittest import TestCase
from bwt import shifter


class TestShifter(TestCase):
    def test_shifter(self):
        in_table = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        shift = 1
        correct_out_table = ['b', 'c', 'd', 'e', 'f', 'g', 'h', 'a']
        out_table = shifter(in_table, shift)

        if not out_table == correct_out_table:
            self.fail()

        in_table = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        shift = 2
        correct_out_table = ['c', 'd', 'e', 'f', 'g', 'h', 'a', 'b']
        out_table = shifter(in_table, shift)

        if not out_table == correct_out_table:
            self.fail()