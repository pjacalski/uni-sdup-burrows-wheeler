from unittest import TestCase
from bwt import bwt_encode


class TestBwt_encode(TestCase):
    def test_bwt_encode(self):
        in_str = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
        correct_str = 'rt,mgmrtt  s ea s rmtncd psleoueaiodclLiiuooin pieicest'
        correct_index = 8

        enc_str, index, pindex = bwt_encode(in_str)

        if not enc_str == correct_str:
            if not correct_index == index:
                self.fail()
