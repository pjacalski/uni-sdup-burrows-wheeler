import copy
import numpy as np


def sorter(in_table):
    array = copy.deepcopy(in_table)
    str_len = len(array)
    id_array = [1, 2, 3, 4, 5, 6, 7, 8]

    for n in range(0, str_len):

        for i in range(0, n):
            if array[i] > array[i + 1]:
                tmp = array[i]
                array[i] = array[i + 1]
                array[i + 1] = tmp

                tmp = id_array[i]
                id_array[i] = id_array[i + 1]
                id_array[i + 1] = tmp

    for i in range(0, str_len):
        pass
    return [id_array, array]


def bubbleSort(in_table):
    array = copy.deepcopy(in_table)

    str_len = len(array)

    id_array = list(np.zeros(str_len))
    id_array = [1, 2, 3, 4, 5, 6, 7, 8]

    for passnum in range(len(array) - 1, 0, -1):
        for i in range(passnum):
            if array[i] > array[i + 1]:
                temp = array[i]
                array[i] = array[i + 1]
                array[i + 1] = temp

                tmp = id_array[i]
                id_array[i] = id_array[i + 1]
                id_array[i + 1] = tmp

    # print(array)

    # for i in range(0, str_len):
    #     for j in range(0, str_len):
    #         if array[i] == in_table[j]:
    #             id_array[j] = i
    return [id_array, array]


def sorter2(in_table):
    array = copy.deepcopy(in_table)
    id_array = [1, 2, 3, 4, 5, 6, 7, 8]

    str_len = len(array)
    n = str_len - 1

    id_out_array = list(np.zeros(str_len))

    while n > 0:
        for i in range(0, n):
            if array[i] > array[i + 1]:
                tmp = array[i]
                array[i] = array[i + 1]
                array[i + 1] = tmp

                tmp = id_array[i]
                id_array[i] = id_array[i + 1]
                id_array[i + 1] = tmp

        n -= 1

    for i in range(0, str_len):
        id_out_array[id_array[i] - 1] = i + 1

    return [id_array, array, id_out_array]


if __name__ == '__main__':
    table = [8, 2, 3, 4, 5, 6, 7, 1]
    table = [3, 8, 4, 2, 5, 1, 6, 7]
    # table = [5, 2, 1, 3, 7, 6, 4, 8]
    # l = [x * 2 for x in l]

    table = [x * 2 for x in table]

    print('table\t\t', table)

    print('id_array\t', sorter2(table)[0])
    print('array\t\t', sorter2(table)[1])
    print('out_array\t', sorter2(table)[2])

    table = [3, 8, 4, 2, 5, 1, 6, 7]
    out_id = [1, 2, 3, 4, 5, 6, 7, 8]
