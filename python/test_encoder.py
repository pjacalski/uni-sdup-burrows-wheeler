from unittest import TestCase
from bwt import encoder


class TestEncoder(TestCase):
    def test_encoder(self):
        input_table = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        index_table = [7, 6, 5, 4, 3, 2, 1, 0]
        correct_out_table = ['h', 'g', 'f', 'e', 'd', 'c', 'b', 'a']
        correct_index = 6

        out_table, out_index = encoder(input_table, index_table)
        if not out_table == correct_out_table:
            if not correct_index == out_index:
                self.fail()
