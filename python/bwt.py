import numpy as np
import copy
import string
from operator import itemgetter


def bwt_encode(input_str):
    str_len = len(input_str)  # get str length
    table = [input_str[i:str_len] + input_str[0:i] for i in range(str_len)]
    table = sorted(table)
    in_str_shift_1 = input_str[1:] + input_str[0]
    index = table.index(input_str)
    prim_idx = table.index(in_str_shift_1) + 1
    last_column = [row[-1:] for row in table]  # last characters of each row
    return "".join(last_column), index, prim_idx  # convert list of characters into string


def bwt_decode(input_string, index):
    str_len = len(input_string)  # get str length
    lookup_table = sorted([(i, x) for i, x in enumerate(input_string)], key=itemgetter(1))
    
    temp_table = [None for i in range(str_len)]
    for i, y in enumerate(lookup_table):
        j, _ = y
        temp_table[j] = i

    out_str_indices = [index]
    for i in range(1, str_len):
        out_str_indices.append(temp_table[out_str_indices[i - 1]])

    out_table = [input_string[i] for i in out_str_indices]
    out_table.reverse()
    return ''.join(out_table)  # convert list of characters into string


def shifter(in_table, shift):
    array = copy.deepcopy(in_table)
    array = array[shift:] + array[:shift]
    return array


def shif_and_get_last(in_table):
    array = copy.deepcopy(in_table)
    array = list(array[-1]) + array[:-1]
    return array


def sorter(in_table):
    array = copy.deepcopy(in_table)
    temp_id_array = [1, 2, 3, 4, 5, 6, 7, 8]

    str_len = len(array)
    n = str_len - 1

    id_array = list(np.zeros(str_len))

    while n > 0:
        for i in range(0, n):
            if array[i] > array[i + 1]:
                tmp = array[i]
                array[i] = array[i + 1]
                array[i + 1] = tmp

                tmp = temp_id_array[i]
                temp_id_array[i] = temp_id_array[i + 1]
                temp_id_array[i + 1] = tmp

        n -= 1

    for i in range(0, str_len):
        id_array[temp_id_array[i] - 1] = i + 1

    return [array, id_array]


def encoder(in_table, index_table):
    array = copy.deepcopy(in_table)
    index = copy.deepcopy(index_table)
    tmp_array = list(np.zeros(len(array)))

    for i in range(0, len(array)):
        tmp_array[index[i] - 1] = array[i]

    array = tmp_array
    prim_index = index[1]

    return [array, prim_index]


def encoder_top(in_table):
    array = copy.deepcopy(in_table)
    metric_array = list(np.zeros(len(array)))

    for i in range(0, len(array)):
        metric_array[i] = str(shifter(array, i))

    last = shif_and_get_last(list(array))

    [array, id_array] = sorter(metric_array)

    out_table, index = encoder(last, id_array)
    return out_table, index


if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--input_str', type=str, help='Input string, len = 8')
    args = parser.parse_args()
    
    in_str = 'Herpderp'
    if args.input_str is not None:
        if len(args.input_str) == 8:
           in_str = args.input_str
        else:
            print('String too long (must be 8 chars), using default..')
    
    print('\n')
    print('input string:\t', in_str)
    enc_str, ind, pidx = bwt_encode(in_str)
    
    print('------ REFERENCE ------')

    print('encoded str:\t', enc_str)
    print('primary index:\t', pidx)
    #print('dec str:\t', bwt_decode(enc_str, ind))
    
    print('------   MODEL   ------')

    print('encoder top:\t', "".join(encoder_top(in_str)[0]))
    print('primary index:\t', encoder_top(in_str)[1])
	
	
