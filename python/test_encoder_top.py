from unittest import TestCase
from bwt import encoder_top


class TestEncoder_top(TestCase):
    def test_encoder_top(self):
        input_table = ['H', 'e', 'r', 'p', 'd', 'e', 'r', 'p']
        correct_out_table = ['p', 'p', 'd','H', 'r', 'r', 'e', 'e']
        correct_index = 0
        out_table, out_index = encoder_top(input_table)

        if not out_table == correct_out_table:
            if not correct_index == out_index:
                self.fail()
