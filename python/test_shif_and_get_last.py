from unittest import TestCase
from bwt import shif_and_get_last


class TestShif_and_get_last(TestCase):
    def test_shif_and_get_last(self):
        in_table = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        correct_out_table = ['h', 'a', 'b', 'c', 'd', 'e', 'f', 'g']
        out_table = shif_and_get_last(in_table)
        if not out_table == correct_out_table:
            self.fail()
