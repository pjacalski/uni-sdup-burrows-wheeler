import bwt
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--input_str', type=str, help='Input string, len = 8')
    args = parser.parse_args()

    in_str = 'Herpderp'
    if args.input_str is not None:
        if len(args.input_str) == 8:
            in_str = args.input_str
        else:
            print('String too long (must be 8 chars), using default..')

    print('\n')
    print('input string:\t', in_str)
    enc_str, ind, pidx = bwt.bwt_encode(in_str)

    print('------ REFERENCE ------')

    print('encoded str:\t', enc_str)
    print('primary index:\t', pidx)
    # print('dec str:\t', bwt.bwt_decode(enc_str, ind))

    print('------   MODEL   ------')

    enc, pidx = bwt.encoder_top(in_str)

    print('encoder top:\t', "".join(enc))
    print('primary index:\t', pidx)
